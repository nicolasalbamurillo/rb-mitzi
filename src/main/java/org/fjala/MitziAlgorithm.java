package org.fjala;

import org.fjala.mitzi.core.RedBlackNode;
import org.fjala.mitzi.core.RedBlackTree;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;

public class MitziAlgorithm {
    public <T, K extends Comparable<K>> RedBlackTree<T, K> solve(RedBlackTree<T, K> tree) {
        RedBlackNode<T, K> nil = tree.getNil();
        RedBlackTree<T, K> result = new RedBlackTree<>(nil);
        Collection<RedBlackNode<T, K>> redNodes = new LinkedList<>();
        tree.getRoot().preOrder(node -> {
            if (node.isRed()) {
                redNodes.add(node);
            }
        });
        redNodes.forEach(node -> result.insert(node.getValue(), node.getKey()));
        return result;
    }
}
