package org.fjala.mitzi.core;

public class RedBlackTree<T, K extends Comparable<K>> {
    private final RedBlackNode<T, K> nil;
    private RedBlackNode<T, K> root;

    public RedBlackTree(RedBlackNode<T, K> nil) {
        this(nil, nil);
    }

    public RedBlackTree(RedBlackNode<T, K> root, RedBlackNode<T, K> nil) {
        this.root = root;
        this.nil = nil;
    }

    public void rotateLeft(RedBlackNode<T, K> x) {
        RedBlackNode<T, K> y = x.right;
        x.right = y.left;
        if (y.left != nil) {
            y.left.parent = x;
        }
        y.parent = x.parent;
        if (x.parent == nil) {
            root = y;
        } else if (x.parent.left == x) {
            x.parent.left = y;
        } else {
            x.parent.right = y;
        }
        y.left = x;
        x.parent = y;
    }

    public void rotateRight(RedBlackNode<T, K> y) {
        RedBlackNode<T, K> x = y.left;
        y.left = x.right;
        if (x.right != nil) {
            x.right.parent = y;
        }
        x.parent = y.parent;
        if (y.parent == nil) {
            root = x;
        } else if (y.parent.left == y) {
            y.parent.left = x;
        } else {
            y.parent.right = x;
        }
        x.right = y;
        y.parent = x;
    }

    public RedBlackNode<T, K> getRoot() {
        return root;
    }

    public void insert(T value, K key) {
        RedBlackNode<T, K> node = new RedBlackNode<>(value, key, nil, nil, nil);
        insert(node);
    }

    public void insert(RedBlackNode<T, K> node) {
        if (root == nil) {
            root = node;
            return;
        }
        root = insert(root, node, nil);
        fixAfterInsert(node);
    }

    private RedBlackNode<T, K> insert(RedBlackNode<T, K> tree, RedBlackNode<T, K> node, RedBlackNode<T, K> parent) {
        if (tree == nil) {
            node.parent = parent;
            node.left = nil;
            node.right = nil;
            node.isRed = true;
            return node;
        }
        int cmp = node.key.compareTo(tree.key);
        if (cmp < 0) {
            tree.left = insert(tree.left, node, tree);
        } else {
            tree.right = insert(tree.right, node, tree);
        }
        return tree;
    }

    private void fixAfterInsert(RedBlackNode<T, K> x) {
        while (x.parent.isRed) {
            if (x.parent == x.parent.parent.left) {
                RedBlackNode<T, K> y = x.parent.parent.right;
                if (y.isRed) {
                    x.parent.isRed = false;
                    y.isRed = false;
                    x.parent.parent.isRed = true;
                    x = x.parent.parent;
                } else {
                    if (x == x.parent.right) {
                        x = x.parent;
                        rotateLeft(x);
                    }
                    x.parent.isRed = false;
                    x.parent.parent.isRed = true;
                    rotateRight(x.parent.parent);
                }
            } else {
                RedBlackNode<T, K> y = x.parent.parent.left;
                if (y.isRed) {
                    x.parent.isRed = false;
                    y.isRed = false;
                    x.parent.parent.isRed = true;
                    x = x.parent.parent;
                } else {
                    if (x == x.parent.left) {
                        x = x.parent;
                        rotateRight(x);
                    }
                    x.parent.isRed = false;
                    x.parent.parent.isRed = true;
                    rotateLeft(x.parent.parent);
                }
            }
        }
        root.isRed = false;
    }

    public RedBlackNode<T, K> getNil() {
        return nil;
    }
}