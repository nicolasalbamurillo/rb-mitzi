package org.fjala.mitzi.core;

public class RedBlackTreeUtils {

    public static boolean isApproximatelyBalanced(RedBlackNode<?, ?> tree) {
        return minPath(tree) * 2 >= maxPath(tree);
    }

    public static int maxPath(RedBlackNode<?, ?> tree) {
        if (tree == null) {
            return 0;
        }
        if (tree.getRight() == null && tree.getLeft() == null) {
            return 1;
        }
        return Math.max(maxPath(tree.getRight()), maxPath(tree.getLeft())) + 1;
    }

    public static int minPath(RedBlackNode<?, ?> tree) {
        if (tree == null) {
            return 0;
        }
        if (tree.getRight() == null && tree.getLeft() == null) {
            return 1;
        }
        return Math.min(maxPath(tree.getRight()), maxPath(tree.getLeft())) + 1;
    }
}
