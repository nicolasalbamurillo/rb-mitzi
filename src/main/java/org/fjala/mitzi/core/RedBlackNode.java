package org.fjala.mitzi.core;

import java.util.function.Consumer;

public class RedBlackNode <T, K extends Comparable<K>> {
    protected T value;
    protected K key;
    protected RedBlackNode<T, K> left;
    protected RedBlackNode<T, K> right;
    protected RedBlackNode<T, K> parent;
    protected boolean isRed;

    public RedBlackNode(T value, K key, RedBlackNode<T, K> parent) {
        this.value = value;
        this.key = key;
        this.parent = parent;
    }

    public RedBlackNode(T value, K key, RedBlackNode<T, K> left, RedBlackNode<T, K> right, RedBlackNode<T, K> parent) {
        this.value = value;
        this.key = key;
        this.left = left;
        this.right = right;
        this.left.parent = this;
        this.right.parent = this;
        this.parent = parent;
    }

    public RedBlackNode(T value, K key, RedBlackNode<T, K> left, RedBlackNode<T, K> right, boolean isRed, RedBlackNode<T, K> parent) {
        this(value, key, left, right, parent);
        this.isRed = isRed;
    }

    public static <T, K extends Comparable<K>> RedBlackNode<T, K> of(T value, K key, RedBlackNode<T, K> nil) {
        return new RedBlackNode<>(value, key, nil, nil, nil);
    }

    public static <T, K extends Comparable<K>> RedBlackNode<T, K> of(T value, K key, boolean isRed, RedBlackNode<T, K> nil) {
        return new RedBlackNode<>(value, key, nil, nil, isRed, nil);
    }

    public static <T, K extends Comparable<K>> RedBlackNode<T, K> of(T value, K key, RedBlackNode<T, K> left, RedBlackNode<T, K> right, RedBlackNode<T, K> nil) {
        return new RedBlackNode<>(value, key, left, right, nil);
    }

    public static <T, K extends Comparable<K>> RedBlackNode<T, K> of(T value, K key, boolean isRed, RedBlackNode<T, K> left, RedBlackNode<T, K> right, RedBlackNode<T, K> nil) {
        return new RedBlackNode<>(value, key, left, right, isRed, nil);
    }

    @Override
    public String toString() {
        if (value == null) {
            return "NIL";
        }
        return value.toString();
    }

    public T getValue() {
        return value;
    }

    public K getKey() {
        return key;
    }

    public RedBlackNode<T, K> getLeft() {
        return left;
    }

    public RedBlackNode<T, K> getRight() {
        return right;
    }

    public RedBlackNode<T, K> getParent() {
        return parent;
    }

    public boolean isRed() {
        return isRed;
    }

    public void preOrder(Consumer<RedBlackNode<T, K>> consumer) {
        preOrder(this, consumer);
    }

    private void preOrder(RedBlackNode<T,K> node, Consumer<RedBlackNode<T,K>> consumer) {
        if (node == null || node.value == null) {
            return;
        }
        consumer.accept(node);
        preOrder(node.left, consumer);
        preOrder(node.right, consumer);
    }
}
