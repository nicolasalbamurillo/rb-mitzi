package org.fjala.mitzi;

import org.fjala.MitziAlgorithm;
import org.fjala.mitzi.core.RedBlackNode;
import org.fjala.mitzi.core.RedBlackTree;
import org.junit.jupiter.api.Test;

import static org.fjala.mitzi.core.RedBlackNode.of;
import static org.junit.jupiter.api.Assertions.*;

class MitziAlgorithmTest {

    private static boolean RED = true;
    private static boolean BLACK = false;

    @Test
    void examplePDF() {
        RedBlackNode<String, String> nil = new RedBlackNode<>(null, null, null);
        RedBlackNode<String, String> root =
                 of("B", "B", BLACK,
                         of("A", "A", BLACK, nil),
                         of("D", "D", BLACK,
                                 of("C", "C", RED, nil),
                                 of("E", "E", RED, nil)
                                 ,nil
                         )
                         ,nil
                 );
        RedBlackTree<String, String> tree = new RedBlackTree<>(root, nil);

        RedBlackTree<String, String> result = new MitziAlgorithm().solve(tree);

        assertEquals("C", result.getRoot().getValue());
        assertFalse(result.getRoot().isRed());
        assertEquals("E", result.getRoot().getRight().getValue());
        assertTrue(result.getRoot().getRight().isRed());
    }
}