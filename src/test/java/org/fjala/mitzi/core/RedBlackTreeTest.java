package org.fjala.mitzi.core;

import org.junit.jupiter.api.Test;

import static org.fjala.mitzi.core.RedBlackTreeUtils.isApproximatelyBalanced;
import static org.junit.jupiter.api.Assertions.*;
import static org.fjala.mitzi.core.RedBlackNode.of;

class RedBlackTreeTest {

    @Test
    void insertShouldInsertNodesAndKeepTheTreeBalanced() {
        RedBlackNode<String, String> nil = new RedBlackNode<>(null, null, null);
        RedBlackTree<String, String> tree = new RedBlackTree<>(of("M", "M", nil), nil);

        tree.insert("D","D");
        tree.insert("F", "F");
        tree.insert("Z", "Z");
        tree.insert("U", "U");
        tree.insert("O", "O");
        tree.insert("Y", "Y");
        tree.insert("B", "B");
        tree.insert("N", "N");
        tree.insert("A", "A");

        assertTrue(isApproximatelyBalanced(tree.getRoot()));
    }
}